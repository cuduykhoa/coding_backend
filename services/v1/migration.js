const path = require('path');
const fs = require('fs');

const { createConnection } = require('../../models');

module.exports.create = async () => {
    const migrationDir = path.join(__dirname, '../../migrations');

    createConnection();

    const filenames = fs.readdirSync(migrationDir);
    const _filenames = filenames.sort();

    for (const filename of _filenames) {
        if (filename !== 'index.js' && filename.slice(-3) === '.js') {
            const migrationFile = require(path.join(migrationDir, filename)); //eslint-disable-line

            if (migrationFile.up) {
                // eslint-disable-next-line no-await-in-loop
                await migrationFile.up();
            }
        }
    }
};
