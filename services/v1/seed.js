const path = require('path');
const fs = require('fs');

const { createConnection } = require('../../models');

module.exports.create = async () => {
    const seedDir = path.join(__dirname, '../../seeds');

    await createConnection();

    const filenames = fs.readdirSync(seedDir);
    const _filenames = filenames.sort();

    for (const filename of _filenames) {
        if (filename !== 'index.js' && filename.slice(-3) === '.js') {
            const seedFile = require(path.join(seedDir, filename)); //eslint-disable-line

            if (seedFile.seed) {
                // eslint-disable-next-line no-await-in-loop
                await seedFile.seed();
            }
        }
    }
};
