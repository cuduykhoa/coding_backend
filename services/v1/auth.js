const { auth } = require('../../shared/firebase');
const { log } = require('../../shared/utils/logging');

const generatePolicy = ({ effect, resources, context = {} }) => ({
    principalId: '*',
    policyDocument: {
        Version: '2012-10-17',
        Statement: [
            {
                Action: 'execute-api:Invoke',
                Effect: effect,
                Resource: resources,
            },
        ],
    },
    context,
});

module.exports.authorizer = async ({
    headers,
    methodArn,
    path,
    requestContext: { httpMethod: method },
}) => {
    try {
        const token = headers.Authorization;

        if (!token) {
            return generatePolicy({ effect: 'Deny', resources: [methodArn] });
        }

        const decodedToken = await auth().verifyIdToken(token);

        if (method === 'PUT' && /\/v\d\/user\/[0-9a-zA-Z]{28}\/?/.test(path)) {
            return generatePolicy({
                effect: 'Allow',
                resources: [methodArn],
                context: {
                    'jwt-userId': decodedToken.uid,
                },
            });
        }

        if (!decodedToken.admin) {
            return generatePolicy({ effect: 'Deny', resources: [methodArn] });
        }

        return generatePolicy({
            effect: 'Allow',
            resources: [methodArn],
            context: {
                'jwt-userId': decodedToken.uid,
            },
        });
    } catch (error) {
        log(error);
        return generatePolicy({ effect: 'Deny', resources: [methodArn] });
    }
};
