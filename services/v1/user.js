const UserModel = require('../../models/user');

module.exports.update = async ({ userId, email, name, birthday }) => {
    const payload = { email };

    if (name) {
        payload.name = name;
    }

    if (birthday) {
        payload.birthday = birthday;
    }

    const user = await UserModel.query().findById(userId);

    if (user) {
        await UserModel.query().findById(userId).patch(payload);
    } else {
        await UserModel.query().insert({ id: userId, ...payload });
    }
};
