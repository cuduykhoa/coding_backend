const AppError = require('../appError');
const { firestore } = require('../../shared/firebase');
const { uploadBySignedUrl, headObject } = require('../../shared/aws/s3');

const BLOG_COLLECTION = 'blog';

module.exports.create = async ({ userId, title, content }) => {
    const blog = await firestore().collection(BLOG_COLLECTION).add({
        title,
        content,
        createdAt: Date.now(),
        updatedAt: Date.now(),
        deletedAt: 0,
        createdBy: userId,
    });

    return {
        id: blog.id,
    };
};

module.exports.index = async () => {
    const rawData = await firestore()
        .collection(BLOG_COLLECTION)
        .where('deletedAt', '==', 0)
        .get();
    const data = rawData.docs.map((doc) => ({ id: doc.id, ...doc.data() }));

    return data;
};

module.exports.update = async ({ blogId, title, content }) => {
    const blog = await firestore()
        .collection(BLOG_COLLECTION)
        .doc(blogId)
        .get();

    if (!blog) {
        throw AppError.BlogNotFound();
    }

    const payload = {
        updatedAt: Date.now(),
    };

    if (title) {
        payload.title = title;
    }

    if (content !== undefined) {
        payload.content = content;
    }

    await firestore().collection(BLOG_COLLECTION).doc(blogId).update(payload);
};

module.exports.show = async ({ blogId }) => {
    const blog = await firestore()
        .collection(BLOG_COLLECTION)
        .doc(blogId)
        .get();

    if (!blog) {
        throw AppError.BlogNotFound();
    }

    return blog;
};

module.exports.delete = async ({ blogId }) => {
    const blog = await firestore()
        .collection(BLOG_COLLECTION)
        .doc(blogId)
        .get();

    if (blog) {
        await firestore().collection(BLOG_COLLECTION).doc(blogId).update({
            deletedAt: Date.now(),
        });
    }
};

module.exports.generateUploadUrl = async ({
    userId,
    blogId,
    fileName,
    contentType,
}) => {
    const filePaths = fileName.split('.');
    const [name, fileType] = filePaths;

    const data = await uploadBySignedUrl({
        bucket: process.env.RESOURCE_BUCKET,
        key: `blog/${Date.now()}.${fileType}`,
        expire: 10 * 60,
        maxSize: 25 * 1024 * 1024,
        conditions: [
            ['eq', '$x-amz-meta-userid', userId],
            ['eq', '$x-amz-meta-id', blogId],
            ['eq', '$x-amz-meta-name', `${name}.${fileType}`],
        ],
        contentType,
    });

    data.fields['x-amz-meta-userid'] = userId;
    data.fields['x-amz-meta-id'] = blogId;
    data.fields['x-amz-meta-name'] = `${name}.${fileType}`;

    return data;
};

module.exports.blogImageUploaded = async ({ event }) => {
    const s3Record = event.Records[0].s3;
    const s3Object = await headObject({
        bucket: s3Record.bucket.name,
        key: s3Record.object.key,
    });
    const { Metadata } = s3Object;
    const { id } = Metadata;
    const payload = {
        image: `https://${process.env.RESOURCE_BUCKET}.s3-${process.env.REGION}.amazonaws.com/${s3Record.object.key}`,
    };

    await firestore().collection(BLOG_COLLECTION).doc(id).update(payload);
};
