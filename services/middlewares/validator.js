const Ajv = require('ajv');
const localize = require('ajv-i18n');

const AppError = require('../appError');

// const customValidator = () => ({
//     arrayStringify: {
//         type: 'string',
//         errors: false,
//         modifying: true,
//         valid: true,
//         compile() {
//             return (data, dataPath, parentData, key) => {
//                 parentData[key] = JSON.parse(data); //eslint-disable-line
//             };
//         },
//     },
// });

// const addKeywords = (...ajvs) => {
//     const _validators = customValidator();

//     for (const [key, opts] of Object.entries(_validators)) {
//         for (const ajv of ajvs) {
//             ajv.addKeyword(key, opts);
//         }
//     }
// };

const validator = ({ schema, event, lang }) => {
    const {
        pathParameters = {},
        queryStringParameters = {},
        body = '{}',
    } = event;

    const _body = typeof body === 'string' ? JSON.parse(body) : body;
    const data = {
        ...JSON.parse(JSON.stringify(queryStringParameters)),
        ..._body,
        ...JSON.parse(JSON.stringify(pathParameters)),
    };

    if (!schema) {
        return {
            ...event,
            requestData: data,
        };
    }

    const { coerceTypes } = schema;
    const ajv = new Ajv({ format: 'full', allErrors: true, $data: true });
    const ajvCoerce = new Ajv({
        $data: true,
        format: 'full',
        allErrors: true,
        coerceTypes: true,
        useDefaults: true,
    });

    // addKeywords(ajv, ajvCoerce);

    const _ajv = coerceTypes ? ajvCoerce : ajv;
    const validate = _ajv.compile(schema);
    const valid = validate(data);

    if (!valid) {
        localize[lang](validate.errors);
        const details = validate.errors.map((e) =>
            `${e.dataPath} ${e.message}`.trim()
        );

        throw AppError.GeneralInvalidParameters(details);
    }

    return {
        ...event,
        requestData: data,
    };
};

module.exports = validator;
