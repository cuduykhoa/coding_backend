const R = require('ramda');

module.exports = (event) => {
    const userId = ['local'].includes(process.env.NODE_ENV)
        ? R.path(['headers', 'jwt-userId'], event)
        : R.path(['requestContext', 'authorizer', 'jwt-userId'], event);

    Object.assign(event.headers, {
        userId,
    });
};
