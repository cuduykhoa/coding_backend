const validator = require('../middlewares/validator');
const { createConnection } = require('../../models');

module.exports = async ({ schema, event, useDBConnection }) => {
    const lang = 'en';
    const _event = validator({ schema, event, lang });

    if (useDBConnection) {
        createConnection();
    }

    return {
        event: _event,
    };
};
