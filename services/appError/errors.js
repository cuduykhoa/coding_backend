const AppError = require('./base');
const HttpStatus = require('./httpStatus');

module.exports = {
    GeneralInternalServerError: (details, stack) =>
        new AppError(
            'GeneralInternalServerError',
            HttpStatus.InternalServerError,
            details,
            stack
        ),
    GeneralBadRequest: () =>
        new AppError('GeneralBadRequest', HttpStatus.BadRequest),
    GeneralForbidden: () =>
        new AppError('GeneralForbidden', HttpStatus.Forbidden),
    GeneralInvalidParameters: (details) =>
        new AppError(
            'GeneralInvalidParameters',
            HttpStatus.BadRequest,
            details
        ),
    BlogNotFound: () => new AppError('BlogNotFound', HttpStatus.NotFound),
};
