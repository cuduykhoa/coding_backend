const errors = require('./errors');
const HttpStatus = require('./httpStatus');

module.exports = {
    ...errors,
    HttpStatus,
};
