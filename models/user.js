const { BaseModel } = require('.');

class UserModel extends BaseModel {
    static get tableName() {
        return 'user';
    }

    static get idColumn() {
        return 'id';
    }
}

module.exports = UserModel;
