const Knex = require('knex');

const { Model } = require('../shared/model');

module.exports = {
    createConnection: () => {
        const knexConnection = Knex({
            client: 'pg',
            connection: process.env.DATABASE_URL,
        });

        Model.knex(knexConnection);
    },
    BaseModel: Model,
};
