const Knex = require('knex');

exports.up = async () => {
    const knex = Knex({
        client: 'pg',
        connection: process.env.DATABASE_URL,
    });

    const userTable = await knex.schema.hasTable('user');

    if (!userTable) {
        await knex.schema.createTable('user', (table) => {
            table.string('id').primary();
            table.string('email');
            table.string('name');
            table.bigInteger('birthday');
            table.bigInteger('deletedAt').defaultTo(0);
            table.timestamp('createdAt').defaultTo(knex.fn.now());
        });
    }
};
