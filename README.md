# backend

## APIs:

-   Update user's profile: PUT - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/user/{userId}
    ```
        properties: {
            userId: { type: 'string' },
            email: { type: 'string', format: 'email' },
            name: { type: 'string', minLength: 1 },
            birthday: { type: 'number', minimum: 0 },
        },
        required: ['email']
    ```
-   Create new article: POST - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blogs
    ```
        properties: {
            title: { type: 'string' },
            content: { type: 'string' },
        },
        required: ['title', 'content']
    ```
-   List articles: GET - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blogs
-   Get an article: GET - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blog/{blogId}
-   Update an article: PUT - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blog/{blogId}
    ```
        properties: {
            blogId: { type: 'string' },
            title: { type: 'string' },
            content: { type: 'string' },
        }
    ```
-   Delete an article: DELETE - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blog/{blogId}
-   Get signed url to upload image: POST - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/blog/attachments
    ```
        properties: {
            blogId: { type: 'string' },
            fileName: { type: 'string' },
            contentType: { type: 'string' },
        },
        required: ['blogId', 'fileName', 'contentType']
    ```
-   POST - https://cxfzy6wneb.execute-api.ap-southeast-1.amazonaws.com/dev/v1/migrations
