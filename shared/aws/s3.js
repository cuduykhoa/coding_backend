const AWS = require('aws-sdk');

const S3 = new AWS.S3({ region: 'ap-southeast-1', signatureVersion: 'v4' });

module.exports.uploadBySignedUrl = ({
    bucket,
    key,
    expire = 60, // 60s
    maxSize = 25000000, // 100Byte - 25MB
    conditions = [],
    contentType,
    acl = 'public-read',
}) => {
    const fields = {
        key,
        'Content-Type': contentType,
    };

    if (acl) {
        fields.acl = acl;
    }

    const params = {
        Bucket: bucket,
        Fields: fields,
        Expires: expire,
        Conditions: [['content-length-range', 100, maxSize], ...conditions],
    };

    return S3.createPresignedPost(params);
};

module.exports.headObject = ({ bucket, key }) => {
    const params = {
        Bucket: bucket,
        Key: key,
    };

    return S3.headObject(params).promise();
};
