/* eslint-disable no-nested-ternary */
/* eslint-disable max-classes-per-file */
const DEFAULT_PAGE_INDEX = 1;
const MAX_ITEM_PER_PAGE = 30;
const DEFAULT_ITEM_PER_PAGE = 30;

module.exports = (Model) => {
    class PaginationQueryBuilder extends Model.QueryBuilder {
        pagination(option = {}) {
            const page = parseInt(option.page, 10);
            const limit = parseInt(option.limit, 10);
            const _page = page ? page - 1 : DEFAULT_PAGE_INDEX;
            const _limit = limit
                ? limit > MAX_ITEM_PER_PAGE
                    ? MAX_ITEM_PER_PAGE
                    : limit
                : DEFAULT_ITEM_PER_PAGE;

            return this.page(_page, _limit);
        }
    }

    return class extends Model {
        static get QueryBuilder() {
            return PaginationQueryBuilder;
        }
    };
};
