const Objection = require('objection');

const pagination = require('./pagination');

class Model extends Objection.mixin(Objection.Model, [pagination]) {}

module.exports = { Model };
