const MigrationService = require('../../services/v1/migration');

const migrationFns = {
    create: async (event, context, callback) => {
        context.callbackWaitsForEmptyEventLoop = false;

        await MigrationService.create();

        callback(null, { body: JSON.stringify({ message: 'ok' }) });
    },
};

module.exports = {
    create: migrationFns.create,
};
