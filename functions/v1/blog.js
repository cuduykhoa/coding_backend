const appWrapper = require('../../services/appWrapper');
const { HttpStatus } = require('../../services/appError');
const BlogService = require('../../services/v1/blog');

const blogFns = {
    create: async ({ event, res }) => {
        const { requestData } = event;
        const { userId } = event.headers;
        const data = await BlogService.create({ ...requestData, userId });

        res.status(HttpStatus.Created).data(data);
    },
    index: async ({ event, res }) => {
        const { requestData } = event;
        const data = await BlogService.index(requestData);

        res.status(HttpStatus.Ok).data(data);
    },
    show: async ({ event, res }) => {
        const { requestData } = event;
        const data = await BlogService.show(requestData);

        res.status(HttpStatus.Ok).data(data);
    },
    update: async ({ event, res }) => {
        const { requestData } = event;
        const { userId } = event.headers;

        await BlogService.update({ ...requestData, userId });

        res.status(HttpStatus.NoContent);
    },
    delete: async ({ event, res }) => {
        const { requestData } = event;
        const { userId } = event.headers;

        await BlogService.delete({ ...requestData, userId });

        res.status(HttpStatus.NoContent);
    },
    generateUploadUrl: async ({ event, res }) => {
        const { requestData } = event;
        const { userId } = event.headers;
        const data = await BlogService.generateUploadUrl({
            ...requestData,
            userId,
        });

        res.status(HttpStatus.Ok).data(data);
    },
    blogImageUploaded: async (event, context, callback) => {
        context.callbackWaitsForEmptyEventLoop = false;

        await BlogService.blogImageUploaded({ event });

        callback(null, { body: JSON.stringify({ message: 'ok' }) });
    },
};

const schema = {
    create: {
        properties: {
            title: { type: 'string' },
            content: { type: 'string' },
        },
        required: ['title', 'content'],
        coerceTypes: true,
        additionalProperties: false,
    },
    index: {
        properties: {},
        coerceTypes: true,
        additionalProperties: false,
    },
    update: {
        properties: {
            blogId: { type: 'string' },
            title: { type: 'string' },
            content: { type: 'string' },
        },
        coerceTypes: true,
        additionalProperties: false,
    },
    show: {
        properties: {
            blogId: { type: 'string' },
        },
        coerceTypes: true,
        additionalProperties: false,
    },
    delete: {
        properties: {
            blogId: { type: 'string' },
        },
        coerceTypes: true,
        additionalProperties: false,
    },
    generateUploadUrl: {
        properties: {
            blogId: { type: 'string' },
            fileName: { type: 'string' },
            contentType: { type: 'string' },
        },
        required: ['blogId', 'fileName', 'contentType'],
        coerceTypes: true,
        additionalProperties: false,
    },
};

module.exports = {
    create: appWrapper({
        fn: blogFns.create,
        schema: schema.create,
    }),
    index: appWrapper({
        fn: blogFns.index,
        schema: schema.index,
    }),
    update: appWrapper({
        fn: blogFns.update,
        schema: schema.update,
    }),
    show: appWrapper({
        fn: blogFns.show,
        schema: schema.show,
    }),
    delete: appWrapper({
        fn: blogFns.delete,
        schema: schema.delete,
    }),
    generateUploadUrl: appWrapper({
        fn: blogFns.generateUploadUrl,
        schema: schema.generateUploadUrl,
    }),
    blogImageUploaded: blogFns.blogImageUploaded,
};
