const AuthService = require('../../services/v1/auth');

const authFns = {
    authorizer: async (event) => {
        const data = await AuthService.authorizer(event);

        return data;
    },
};

module.exports = {
    authorizer: authFns.authorizer,
};
