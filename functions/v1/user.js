const appWrapper = require('../../services/appWrapper');
const { HttpStatus } = require('../../services/appError');
const UserService = require('../../services/v1/user');

const userFns = {
    update: async ({ event, res }) => {
        const { userId } = event.headers;
        const { requestData } = event;

        await UserService.update({ ...requestData, userId });

        res.status(HttpStatus.NoContent);
    },
};

const schema = {
    update: {
        properties: {
            userId: { type: 'string' },
            email: { type: 'string', format: 'email' },
            name: { type: 'string', minLength: 1 },
            birthday: { type: 'number', minimum: 0 },
        },
        required: ['email'],
        coerceTypes: true,
        additionalProperties: false,
    },
};

module.exports = {
    update: appWrapper({
        fn: userFns.update,
        schema: schema.update,
        useDBConnection: true,
    }),
};
