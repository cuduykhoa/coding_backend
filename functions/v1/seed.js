const SeedService = require('../../services/v1/seed');

const seedFns = {
    create: async (event, context, callback) => {
        context.callbackWaitsForEmptyEventLoop = false;

        await SeedService.create();

        callback(null, { body: JSON.stringify({ message: 'ok' }) });
    },
};

module.exports = {
    create: seedFns.create,
};
